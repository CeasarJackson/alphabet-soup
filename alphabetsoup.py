#!/bin/python3
# Ceasar Jackson Alphabetsoup challenge
import re
import sys

# This function searches for each keyword in the list and gives coordinates of currnet search location
# and current number of searches (tries) in that direction
def look_for_word(location,l):
    return (location[0] * l, location[1] * l)

# This function gives coordinates of the word_index (clues) starting and curent position(s)
def index_postion(x, y):
    return (x[0] + y[0], x[1] + y[1])

def search_for_word(word_seacrh, keys):
    # horizontally, vertically, diagonally, and reversed directions
    directions = [[1, 0], [0, 1], [1, 1], [1, -1], [-1, 0], [0, -1], [-1, -1], [-1, 1]]
    for keyword in word_seacrh:
        word_length = len(keyword)
        potential_word = ''
        if keyword [0] not in keys:
            break
        for potential_letter in keys[keyword[0]]:
            for location in directions:
                word = []
                for i in range(word_length): # searches textfile for word
                    x, y = index_postion(potential_letter, look_for_word(location, i))
                    if x < 0 or x > rows -1 or y < 0 or y > cols -1:
                        break
                    word.append(matrix[x][y])
                potential_word = ''.join(word)
                if (keyword  == potential_word):
                    print(f'{keyword } {potential_letter[0]}:{potential_letter[1]} {x}:{y}') # formatted string literal +> 3.6+
                    break
    pass

if __name__ == '__main__':

    grid_size = input()
    matrix = []
    keys = {}
    # The find() method finds the first occurrence of the specified value:  string.find(value, start, end)
    row_location = grid_size.find('x')
    rows = int(grid_size[0:row_location])
    cols = int(grid_size[row_location + 1 : len(grid_size)])

    for i in range(rows):
        matrix.append(list(map(str, input().strip().split())))  # typecast matrix
    # find keywords in texfile
    data = sys.stdin.readlines()
    word_seacrh = (list(map(lambda x: x.strip(), data)))   # typecast Word_search list

    for i in range(rows):
        for j in range(cols):
            if matrix[i][j] not in keys:
                keys[matrix[i][j]] = []
            keys[matrix[i][j]].append((i,j))
    search_for_word(word_seacrh, keys)
